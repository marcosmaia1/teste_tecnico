-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.21-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para teste_tecnico
CREATE DATABASE IF NOT EXISTS `teste_tecnico` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `teste_tecnico`;

-- Copiando estrutura para tabela teste_tecnico.documento
CREATE TABLE IF NOT EXISTS `documento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Numero` bigint(20) NOT NULL,
  `Total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Confirmado` int(11) NOT NULL DEFAULT '0',
  `Criacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Documento_usuario` (`usuario_id`),
  CONSTRAINT `FK_Documento_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela teste_tecnico.documento: ~23 rows (aproximadamente)
/*!40000 ALTER TABLE `documento` DISABLE KEYS */;
INSERT INTO `documento` (`id`, `Numero`, `Total`, `Confirmado`, `Criacao`, `usuario_id`) VALUES
	(27, 27, 2.22, 0, '2018-10-31 09:07:42', 3),
	(28, 28, 2.22, 0, '2018-10-31 09:35:04', 3),
	(29, 29, 4.00, 0, '2018-10-31 09:37:10', 3),
	(30, 30, 4.00, 0, '2018-10-31 09:38:57', 3),
	(31, 31, 2.00, 0, '2018-10-31 09:40:15', 3),
	(32, 32, 0.00, 0, '2018-10-31 09:41:21', 3),
	(33, 33, 2.22, 0, '2018-10-31 09:44:05', 3),
	(34, 34, 2.22, 0, '2018-10-31 09:44:51', 3),
	(35, 35, 2.22, 0, '2018-10-31 09:48:13', 3),
	(36, 36, 2.22, 0, '2018-10-31 09:48:46', 3),
	(37, 37, 2.22, 0, '2018-10-31 09:50:19', 3),
	(38, 38, 2.22, 0, '2018-10-31 09:52:03', 3),
	(39, 39, 2.22, 0, '2018-10-31 09:52:20', 3),
	(40, 40, 2.22, 2, '2018-10-31 09:53:58', 3),
	(41, 41, 2.22, 1, '2018-10-31 10:12:17', 3),
	(42, 42, 2.22, 0, '2018-10-31 10:17:42', 3),
	(43, 0, 2.22, 0, '2018-10-31 10:18:11', 3),
	(44, 0, 2.22, 0, '2018-10-31 10:18:13', 3),
	(45, 0, 2.22, 0, '2018-10-31 10:18:25', 3),
	(46, 46, 2.22, 0, '2018-10-31 10:18:41', 3),
	(47, 47, 2.22, 0, '2018-10-31 10:18:51', 3),
	(48, 48, 2.22, 0, '2018-10-31 10:19:26', 3),
	(49, 49, 2.22, 0, '2018-10-31 10:19:35', 3),
	(50, 50, 11.37, 1, '2018-10-31 10:20:07', 3),
	(51, 51, 18.68, 1, '2018-10-31 10:23:09', 3);
/*!40000 ALTER TABLE `documento` ENABLE KEYS */;

-- Copiando estrutura para tabela teste_tecnico.item
CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto_id` int(11) NOT NULL DEFAULT '0',
  `documento_id` int(11) NOT NULL DEFAULT '0',
  `usuario_id` int(11) NOT NULL DEFAULT '0',
  `Quantidade` int(11) NOT NULL DEFAULT '0',
  `Preco` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Criacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_item_produto` (`produto_id`),
  KEY `FK_item_documento` (`documento_id`),
  KEY `FK_item_usuario` (`usuario_id`),
  CONSTRAINT `FK_item_documento` FOREIGN KEY (`documento_id`) REFERENCES `documento` (`id`),
  CONSTRAINT `FK_item_produto` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`id`),
  CONSTRAINT `FK_item_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela teste_tecnico.item: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` (`id`, `produto_id`, `documento_id`, `usuario_id`, `Quantidade`, `Preco`, `Criacao`) VALUES
	(26, 12, 27, 3, 2, 2.00, '2018-10-31 09:07:42'),
	(27, 12, 28, 3, 1, 2.00, '2018-10-31 09:35:04'),
	(28, 12, 29, 3, 1, 2.00, '2018-10-31 09:37:10'),
	(29, 12, 29, 3, 1, 2.00, '2018-10-31 09:37:47'),
	(30, 12, 30, 3, 1, 2.00, '2018-10-31 09:38:57'),
	(31, 12, 30, 3, 1, 2.00, '2018-10-31 09:39:31'),
	(32, 12, 31, 3, 1, 2.00, '2018-10-31 09:40:15'),
	(34, 12, 32, 3, 1, 2.00, '2018-10-31 09:42:21'),
	(35, 12, 33, 3, 1, 2.00, '2018-10-31 09:44:05'),
	(36, 12, 34, 3, 1, 2.00, '2018-10-31 09:44:51'),
	(37, 12, 34, 3, 0, 2.00, '2018-10-31 09:44:53'),
	(38, 12, 35, 3, 1, 2.00, '2018-10-31 09:48:13'),
	(39, 12, 35, 3, 0, 2.00, '2018-10-31 09:48:15'),
	(40, 12, 36, 3, 1, 2.00, '2018-10-31 09:48:46'),
	(41, 12, 36, 3, 0, 2.00, '2018-10-31 09:48:48'),
	(42, 12, 37, 3, 1, 2.00, '2018-10-31 09:50:20'),
	(43, 12, 37, 3, 0, 2.00, '2018-10-31 09:50:21'),
	(44, 12, 37, 3, 0, 2.00, '2018-10-31 09:51:59'),
	(45, 12, 38, 3, 1, 2.00, '2018-10-31 09:52:03'),
	(46, 12, 39, 3, 1, 2.00, '2018-10-31 09:52:21'),
	(51, 12, 40, 3, 2, 2.00, '2018-10-31 09:55:54'),
	(52, 12, 41, 3, 3, 2.00, '2018-10-31 10:12:17'),
	(53, 13, 41, 3, 2, 2.00, '2018-10-31 10:12:24'),
	(54, 12, 42, 3, 1, 2.00, '2018-10-31 10:17:42'),
	(55, 12, 50, 3, 3, 2.22, '2018-10-31 10:20:07'),
	(56, 13, 50, 3, 3, 1.57, '2018-10-31 10:20:14'),
	(57, 12, 51, 3, 7, 2.22, '2018-10-31 10:23:09'),
	(58, 13, 51, 3, 2, 1.57, '2018-10-31 10:23:16');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;

-- Copiando estrutura para tabela teste_tecnico.produto
CREATE TABLE IF NOT EXISTS `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Codigo` bigint(20) DEFAULT NULL,
  `Descricao` varchar(50) NOT NULL,
  `Preco` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Criacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_produto_usuario` (`usuario_id`),
  CONSTRAINT `FK_produto_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela teste_tecnico.produto: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` (`id`, `Codigo`, `Descricao`, `Preco`, `Criacao`, `usuario_id`) VALUES
	(12, 123, 'Teste 1', 2.22, '2018-10-30 19:27:11', 3),
	(13, 234, 'Chiclete', 1.57, '2018-10-31 10:11:39', 3);
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;

-- Copiando estrutura para tabela teste_tecnico.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) NOT NULL,
  `Login` varchar(10) NOT NULL,
  `Password` varchar(128) NOT NULL,
  `Criacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Tipo` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Login` (`Login`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela teste_tecnico.usuario: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `Nome`, `Login`, `Password`, `Criacao`, `Tipo`) VALUES
	(1, 'ROOT', 'root', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '2018-10-29 22:41:41', 0),
	(2, 'Vendedor Teste', 'vendedor', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '2018-10-30 05:18:41', 2),
	(3, 'Administrador Teste', 'admin', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '2018-10-30 05:19:02', 1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
