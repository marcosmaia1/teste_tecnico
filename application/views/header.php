<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="author" content="Marcos Maia">
        <meta name="description" content="Teste Técnico - Sistema de vendas">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <?php
            $base_url = base_url();
            if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
                $base_url =  'https://' . substr($base_url, 7);
            }
        ?>
        <title>Teste Técnico - Sistema de vendas</title>

        <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>includes/default/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>includes/default/fontawesome/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>includes/default/css/dashboard.css">
        <!-- Google Fonts -->
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic,700italic|Montserrat:400,700">
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url('dashboard'); ?>">Sistema de vendas</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo base_url('produto/index'); ?>">Produtos</a></li>
                        <li><a href="<?php echo base_url('venda/index'); ?>">Vendas</a></li>
                        <li><a href="<?php echo base_url('home/logout'); ?>">Sair</a></li>
                    </ul>
                </div>
            </div>
        </nav>
