<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="author" content="Marcos Maia">
        <meta name="description" content="Teste Técnico - Sistema de vendas">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <?php
            $base_url = base_url();
            if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
                $base_url =  'https://' . substr($base_url, 7);
            }
        ?>
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

        <title>Teste Técnico - Sistema de vendas</title>

        <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>includes/default/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>includes/default/fontawesome/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>includes/default/css/dashboard.css">
        <!-- Google Fonts -->
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic,700italic|Montserrat:400,700">
    </head>
    <body>
        <section id="formulario" class="formulario-section transparent" data-type="background" data-parallax="yes">
            <div class="overlay section-padding" style="background: white;">
                <div class="container">
                    <h1 class="section-title text-center text-uppercase"><span> Sistema de vendas</span></h1>
                    <!-- /.section-description -->
                    <div class="top-padding-50"></div>
                    <!-- /.col-md-8 -->
                    <div class="col-md-8 col-md-offset-2 col-sm-12">
                        <div class="formulario-container">
                            <form method="post" action="<?php echo base_url('home/loginPost'); ?>" class="login-form">
                                <span class="text-danger"><?php echo isset($error)? $error : ''; ?></span>
                                <?php echo isset($result)? var_dump($result) : ''; ?>
                                <div class="form-group">
                                    <label for="login">Login</label>
                                    <input type="text" class="form-control" id="login" name="login" placeholder="Digite seu login..." required />
                                </div>

                                <div class="form-group">
                                    <label for="senha">Senha</label>
                                    <input type="password" class="form-control" id="senha" name="senha" placeholder="*******" required />
                                </div>
                                <button type="submit" class="btn btn-default btn-lg"> <span class="fa fa-sign-in"></span> Entrar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        	include APPPATH."views\scripts.php";
        ?>
    </body>
</html>
