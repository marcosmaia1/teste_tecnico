<?php
    $base_url = base_url();
    if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
      $base_url =  'https://' . substr($base_url, 7);
    }
?>
<script type="text/javascript" src="<?php echo $base_url; ?>includes/default/jquery/jquery.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>includes/default/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>includes/default/js/jquery.maskMoney.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>includes/default/js/dashboard.js"></script>
