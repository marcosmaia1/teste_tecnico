<!-- <div class="modal fade" id="produto_novo_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false"> -->
<style>
    .centerItem {
        text-align: center;
    }
    .rightItem {
        text-align: right;
    }
}
</style>
<div class="modal fade bd-example-modal-lg" id="venda_novo_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form id="venda_novo_modal_form" action="<?php echo base_url('venda/venda_salvar'); ?>" class="form-horizontal" role="form" method="post">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="venda_numero">Nova venda</h4>
                    <input type="hidden" value="" id="venda_id" name="venda_id">
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <h4>Produto</h4>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <label class="control-label" for="form-field-1">&nbsp;</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Digite o código do produto" type="number" min="0" max="9999999999" id="produto_codigo" name="produto_codigo" value="0">
                                        <span class="input-group-btn">
                                          <button class="btn btn-default" type="button" id="procurar_produto_btn">Procurar</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <h4 >Lista de Produtos</h4>
                                <div class="" style="height:60vh; overflow-y: auto;">
                                    <table class="table" id="tabela_produto">
                                        <thead>
                                            <tr>
                                                <th class="item_data" style="width: 60vh;">Item</th>
                                                <th class="centerItem" style="width: 40vh;">Qtd</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tabela_produto_item">
                                            <!-- <tr id="item_0">
                                                <td>
                                                <b>Código:</b> 6666666666 <br>
                                                <b>Descrição:</b> Cerveja Cerveja Cerveja Cerveja Cerveja <br>
                                                <b>Preço:</b> 3332,50 <br>
                                                </td>
                                                <td class="centerItem">
                                                    <a class="red delButton" onclick="delItem(0)">
                                                        <i class="glyphicon glyphicon-minus-sign"></i>
                                                    </a>
                                                    <input class="tableItem centerItem" id="item_qtd_0" style="width:30px;" type="text" value="1"/>
                                                    <a class="green addButton" onclick="addItem(0)">
                                                        <i class="glyphicon glyphicon-plus-sign"></i>
                                                    </a>
                                                </td>
                                            </tr> -->
                                        </tbody>

                                        <!-- <tbody id="tabela_produto_item">
                                            <tr id="item_0">
                                                <td>6666666666</td>
                                                <td>Cerveja Cerveja Cerveja Cerveja Cerveja</td>
                                                <td class="rightItem">3332,50</td>
                                                <td class="centerItem">
                                                    <a class="red delButton" onclick="delItem(0)">
                                                        <i class="glyphicon glyphicon-minus-sign"></i>
                                                    </a>
                                                    <input class="tableItem centerItem" id="item_qtd_0" style="width:30px;" type="number" min="0" value="1"/>
                                                    <a class="green addButton" onclick="addItem(0)">
                                                        <i class="glyphicon glyphicon-plus-sign"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody> -->
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="salvarVenda" disabled class="btn btn-primary">
                            Confirmar venda
                        </button>
                        <button type="button" id="cancelarVenda" class="btn btn-default" data-dismiss="modal">
                            Cancelar venda
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="container-fluid">
    <div class="row">
        <!-- <div class="col-sm-12 col-md-12 main"> -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main">
            <div class="row">
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                    <a href="#" data-toggle="modal" data-target="#venda_novo_modal">
                        <div class="btn btn-lg btn-success" >
                            <h1><span class="fa fa-sign-in"></span></h1>
                            <span>Nova venda</span>
                        </div>
                    </a>
                </div>
            </div>
            <h3 class="sub-header">Lista de Vendas</h3>
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Número</th>
                                <th>Confirmado</th>
                                <th>Usuario</th>
                                <th>Criação</th>
                                <th>Total</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($lista as $item){ ?>
                                <tr>
                                    <td><?php echo $item->Numero; ?></td>
                                    <td><?php echo $item->Confirmado; ?></td>
                                    <td><?php echo $item->usuario_id; ?></td>
                                    <td><?php echo $item->Criacao; ?></td>
                                    <td><?php echo $item->Total; ?></td>
                                    <td>
                                        <a class="green addButton" onclick="addItem(0)">
                                            <i class="glyphicon glyphicon-plus-sign"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total de vendas confirmadas:<b></td>
                                <td><?php echo $total; ?></td>
                                <td></td>
                            </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
	include APPPATH."views\scripts.php";
?>
<script type="text/javascript">
    $(window).ready(function () {
        loadingInit();
        $("#produto_preco").maskMoney({allowZero: true, symbol: '', showSymbol: false, thousands: '.', decimal: ',', symbolStay: true});

    <?php
    if(isset($open_novo_modal) && $open_novo_modal){
        echo "
        $('#venda_novo_modal').modal('toggle');
        $('#venda_novo_modal').on('shown.bs.modal', function() {
            $('#produto_codigo').focus();
            $('#produto_codigo').select();
        });
        ";
    }
    ?>
        $('#venda_novo_modal').on('hide.bs.modal', function() {
            limparVenda();
        });

        $('.tableItem').focus(function(){
            $(this).select();
        });
        $('.tableItem').keyup(function(){
            var qtd = $(this).val();
            qtd = somenteNumeros(qtd);
            qtd = qtd != '' ? qtd : 1;
            $(this).val(qtd);
        });
        $('.tableItem').blur(function(){
            var qtd = $(this).val();
            qtd = somenteNumeros(qtd);
            qtd = qtd != '' ? qtd : 1;
            $(this).val(qtd);
            //adicionaProduto($(this).data('id'), qtd, $('#venda_id').val());
        });
    });

    $('#salvarVenda').on('click', function(){
        salvar(1);
    });
    $('#cancelarVenda').on('click', function(){
        salvar(2);
    });

    function limparVenda(){
        $('#produto_codigo').val(0);
        $('#venda_id').val('');
        $('#venda_numero').text('Nova venda');
        $('#tabela_produto_item').html('');
        $('#salvarVenda').attr('disabled', true);
    }

    function salvar(status){
        if($('#venda_id').val() == ''){
            limparVenda();
            return true;
        }
        loadingShow();

        $.ajax({
            url: '<?php echo $base_url; ?>venda/updatestatusvenda/'+$('#venda_id').val()+'/'+status,
            contentType: false,
            processData: false,
            success: function (response) {
                try {
                    console.log(response);
                    if(typeof response !== 'object'){
                        response = jQuery.parseJSON(response);
                    }
                    if(response['ok']){
                        alert(response['data']);
                        window.location = '<?php echo $base_url; ?>venda/index/1';
                    } else {
                        alert(response['error']);
                    }
                    loadingHide();
                } catch(err) {
                    alert('Ops, aconteceu alguma coisa. Por favor, tente novamente.');
                    console.log(err);
                    console.log(err.responseText);
                    loadingHide();
                }
            },
            error: function(xhr, status, error){
                alert('Ops, aconteceu alguma coisa. Por favor, tente novamente.');
                console.log(xhr);
                console.log(status);
                console.log(error);
                loadingHide();
            }
        });
    }
    $('#procurar_produto_btn').click(function(){
        adicionaProduto($('#produto_codigo').val(), 1, $('#venda_id').val());
    });

    function adicionaProduto(codigo, qtd,venda_id){
        loadingShow();
        if(venda_id != ''){
            venda_id = '/'+venda_id;
        }
        $.ajax({
            type: 'GET',
            url: '<?php echo $base_url; ?>venda/adicionaproduto/'+codigo+'/'+qtd+venda_id,
            contentType: false,
            processData: false,
            success: function (response) {
                try {
                    console.log(response);
                    if(typeof response !== 'object'){
                        response = jQuery.parseJSON(response);
                    }
                    if(response['ok']){
                        response = response['data'];
                        $('#produto_codigo').val(0);
                        $('#venda_id').val(response['venda']['id']);
                        $('#venda_numero').text('Venda atual: '+response['venda']['Numero']);
                        adicionaProdutoTabela(response['produto']);
                        $('#salvarVenda').removeAttr('disabled');
                    } else {
                        alert(response['error']);
                    }
                    loadingHide();
                } catch(err) {
                    alert('Ops, aconteceu alguma coisa. Por favor, tente novamente.');
                    console.log(err);
                    console.log(err.responseText);
                    loadingHide();
                }
            },
            error: function(xhr, status, error){
                alert('Ops, aconteceu alguma coisa. Por favor, tente novamente.');
                console.log(xhr);
                console.log(status);
                console.log(error);
                loadingHide();
            }
        });
        $('#produto_codigo').focus();
        $('#produto_codigo').select();
    }

    function adicionaProdutoTabela(produtoObject){
        var qtd = $('.produto_item').length;

        if($('#item_'+produtoObject['produto_id']).length > 0){
            $('#item_qtd_'+produtoObject['produto_id']).val(produtoObject['Quantidade']);
            return true;
        }

        var model = ''+
        '<tr class="produto_item" id="item_'+produtoObject['produto_id']+'">'+
            // '<td>'+produtoObject['Codigo']+'</td>'+
            // '<td>'+produtoObject['Descricao']+'</td>'+
            // '<td class="rightItem">'+produtoObject['Preco']+'</td>'+
            '<td>'+
                '<b>Código:</b> '+produtoObject['Codigo']+'<br>'+
                '<b>Descrição:</b> '+produtoObject['Descricao']+'<br>'+
                '<b>Preço:</b> '+produtoObject['Preco']+'<br>'+
            '</td>'+
            '<td class="centerItem">'+
                '<a class="red delButton" onclick="delItem('+produtoObject['produto_id']+')">'+
                    '<i class="glyphicon glyphicon-minus-sign"></i>'+
                '</a> '+
                '<input class="tableItem centerItem" disabled id="item_qtd_'+produtoObject['produto_id']+'" data-id="" type="text" style="width:40px;" value="'+produtoObject['Quantidade']+'"/>'+
                ' <a class="green addButton" data-row="" onclick="addItem('+produtoObject['produto_id']+','+produtoObject['Codigo']+')">'+
                    '<i class="glyphicon glyphicon-plus-sign"></i>'+
                '</a> '+
            '</td>'+
        '</tr>';
        $('#tabela_produto_item').append(model);
    }

    function delItem(item, codigo){
        var qtd = $('#item_qtd_'+item).val();
        if(qtd == 1 || qtd == 0){
            removeProduto(item, $('#venda_id').val());
            if($('.produto_item').length == 0 || $('.produto_item').length == undefined){
                $('#salvarVenda').attr('disabled', true);
            }
        } else {
            adicionaProduto(codigo, qtd, $('#venda_id').val());
        }
    }

    function addItem(item, codigo){
        var qtd = parseInt($('#item_qtd_'+item).val()) + 1;
        adicionaProduto(codigo, qtd, $('#venda_id').val());
    }

    function removeProduto(codigo, venda_id){
        if(venda_id == ''){
            $('#item_'+codigo).remove();
            return true;
        }

        loadingShow();
        $.ajax({
            type: 'GET',
            url: '<?php echo $base_url; ?>venda/removeproduto/'+codigo+'/'+venda_id,
            contentType: false,
            processData: false,
            success: function (response) {
                try {
                    console.log(response);
                    if(typeof response !== 'object'){
                        response = jQuery.parseJSON(response);
                    }
                    if(response['ok']){
                        loadingHide();
                        $('#item_'+codigo).remove();
                    } else {
                        alert(response['error']);
                    }
                    loadingHide();
                } catch(err) {
                    alert('Ops, aconteceu alguma coisa. Por favor, tente novamente.');
                    console.log(err);
                    console.log(err.responseText);
                    loadingHide();
                }
            },
            error: function(xhr, status, error){
                alert('Ops, aconteceu alguma coisa. Por favor, tente novamente.');
                console.log(xhr);
                console.log(status);
                console.log(error);
                loadingHide();
            }
        });
        $('#produto_codigo').focus();
        $('#produto_codigo').select();
    }

</script>
