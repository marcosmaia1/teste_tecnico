<!-- <div class="modal fade" id="produto_novo_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false"> -->
<div class="modal fade" id="produto_novo_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form id="produto_novo_modal_form" action="<?php echo base_url('produto/produto_salvar'); ?>" class="form-horizontal" role="form" method="post">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Novo Produto</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
                                <label class="control-label" for="form-field-1">Código</label>
                                <div class="input-control text" data-role="input-control">
                                    <input class="form-control" placeholder="Código do produto" type="number" min="0" max="9999999999" maxlength="10" id="produto_codigo" name="produto_codigo" value="0">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
                                <label class="control-label" for="form-field-1">Descrição</label>
                                <div class="input-control text" data-role="input-control">
                                    <input class="form-control" placeholder="Descrição do produto" maxlength="45" type="text" id="produto_descricao" name="produto_descricao" value="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
                                <label class="control-label" for="form-field-1">Preço</label>
                                <div class="input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input class="form-control" type="text" id="produto_preco" maxlength="12" name="produto_preco" value="0,00">
                                </div>
                            </div>
                    </div>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="salvarProduto" class="btn btn-primary">
                        Salvar Produto
                    </button>
                    <button type="button" id="fecharModal" class="btn btn-default" data-dismiss="modal">
                        Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
</div>

<div class="container-fluid">
    <div class="row">
        <!-- <div class="col-sm-12 col-md-12 main"> -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main">
            <div class="row">
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                    <a href="#" data-toggle="modal" data-target="#produto_novo_modal">
                        <div class="btn btn-lg btn-success" >
                            <h1><span class="fa fa-sign-in"></span></h1>
                            <span>Novo produto</span>
                        </div>
                    </a>
                </div>
            </div>
            <h3 class="sub-header">Lista de Produtos</h3>
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Codigo</th>
                                <th>Descricao</th>
                                <th>Preco</th>
                                <th>Usuario</th>
                                <th>Criacao</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($lista as $item){ ?>
                                <tr>
                                    <td><?php echo $item->id; ?></td>
                                    <td><?php echo $item->Codigo; ?></td>
                                    <td><?php echo $item->Descricao; ?></td>
                                    <td><?php echo $item->Preco; ?></td>
                                    <td><?php echo $item->usuario_id; ?></td>
                                    <td><?php echo $item->Criacao; ?></td>
                                    <td></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
	include APPPATH."views\scripts.php";
?>
<script type="text/javascript">
    $(window).ready(function () {
        $("#produto_preco").maskMoney({allowZero: true, symbol: '', showSymbol: false, thousands: '.', decimal: ',', symbolStay: true});
        loadingInit();
    <?php
    if(isset($open_novo_modal) && $open_novo_modal){
        echo "
        $('#produto_novo_modal').modal('toggle');
        $('#produto_novo_modal').on('shown.bs.modal', function() {
            $('#produto_codigo').focus();
            $('#produto_codigo').select();
        });
        ";
    }
    ?>
    });

    $('#produto_novo_modal').on('shown.bs.modal', function() {
        $('#produto_codigo').focus();
    });

    $('#produto_novo_modal_form').on('submit', salvar);

    function salvar(e){
        e.preventDefault();
        loadingShow();

        var data = new FormData(e.target);
        $.ajax({
            type: 'POST',
            url: '<?php echo $base_url; ?>produto/produto_salvar',
            data: data,
            contentType: false,
            processData: false,
            success: function (response) {
                try {
                    console.log(response);
                    if(typeof response !== 'object'){
                        response = jQuery.parseJSON(response);
                    }
                    if(response['ok']){
                        alert('Produto cadastrado com sucesso.');
                        window.location = '<?php echo $base_url; ?>produto/index/1';
                    } else {
                        alert(response['error']);
                    }
                    loadingHide();
                } catch(err) {
                    alert('Ops, aconteceu alguma coisa. Por favor, tente novamente.');
                    console.log(err);
                    console.log(err.responseText);
                    loadingHide();
                }
            },
            error: function(xhr, status, error){
                alert('Ops, aconteceu alguma coisa. Por favor, tente novamente.');
                console.log(xhr);
                console.log(status);
                console.log(error);
                loadingHide();
            }
        });
    }

    $('#produto_codigo').blur(function(){
        if($('#produto_codigo').val().length>=10){
            $('#produto_codigo').val('0');
        }
    });
</script>
