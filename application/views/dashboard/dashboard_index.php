<div class="container-fluid">
    <div class="row">
        <!-- <div class="col-sm-12 col-md-12 main"> -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main">
            <h1 class="page-header">Acesso rápido</h1>
            <div class="row">
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                    <a href="<?php echo base_url('venda/index/1'); ?>">
                        <div class="btn btn-lg btn-success" >
                            <h1><span class="fa fa-sign-in"></span></h1>
                            <span>Nova venda</span>
                        </div>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2">
                    <a href="<?php echo base_url('produto/index/1'); ?>">
                        <div class="btn btn-lg btn-primary" >
                            <h1><span class="fa fa-sign-in"></span></h1>
                            <span>Novo produto</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
	include APPPATH."views\scripts.php";
?>
