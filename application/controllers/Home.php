<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
	 	parent::__construct();
		$this->load->helper('url');
		$this->load->model('Usuario_model');
	}

	public function index()
	{
		$this->load->view('login/login_index');
	}

	public function loginPost()
	{
		$post = $this->input->post();
		$result = $this->Usuario_model->login($post['login'], hash('sha256', $post['senha']));
		if($result == null){
			$data = array(
				'error' => 'Credenciais incorretas',
				'result' => $result
			);
			$this->load->view('login/login_index', $data);
		} else {
			unset($result->Password);
			$session_data = array(
				'usuario' => $result
			);
			$this->session->set_userdata($session_data);
			$dados = array(
				'result' => $this->session->all_userdata()
			);
			redirect('dashboard');
		}
	}

	public function logout(){
       $this->session->unset_userdata('usuario');
	   $this->session->sess_destroy();
       $this->load->view('login/login_index');
	}
}
