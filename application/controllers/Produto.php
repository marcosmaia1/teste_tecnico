<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH."helpers/Util.php");

class Produto extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
	 	parent::__construct();
		$this->load->helper('url');
		$this->load->model('Produto_model');
	}

	public function Check_session(){
		$session = $this->session->all_userdata();
		if(!isset($session['usuario'])){
			if($this->input->is_ajax_request()){
				header('Locaction: '.base_url());
			} else {
				$this->load->view('login/login_index');
			}
			return false;
		}
		return true;
	}

	public function index($novo = 0)
	{
		if($this->Check_session()){
			$dados = array(
				'session_data' => $this->session->all_userdata()
			);

			$page = array(
				'open_novo_modal' => $novo == 1 ? true : false,
				'lista' => $this->Produto_model->listAll()
			);
			$this->load->view('header', $dados);
			$this->load->view('produto/produto_index', $page);
			$this->load->view('footer');
		}
	}

	public function Produto_salvar()
	{
		if($this->Check_session()){
			$post = $this->input->post();
			#$errors = [];
			$errors = '';
			$produto = array(
				'Codigo' => (int) $post['produto_codigo'],
				'Descricao' => trim($post['produto_descricao']),
				'Preco' => Util::Formatar_valores($post['produto_preco']),
				'Usuario_id' => $this->session->all_userdata()['usuario']->id
			);

			if($produto['Codigo'] == '' || $produto['Codigo'] == 0){
				// $errors[] = 'Descição não pode estar vazia.';
				$errors .= "Código deve ser um valor válido. \n";
			}

			if($produto['Codigo'] != ''){
				// $errors[] = 'Descição não pode estar vazia.';
				$dup = $this->Produto_model->GetByCodigo($produto['Codigo']);
				if($dup != null){
					$errors .= "Código já está sendo utilizado. \n";
				}
			}

			if($produto['Descricao'] == ''){
				// $errors[] = 'Descição não pode estar vazia.';
				$errors .= "Descição não pode estar vazia. \n";
			}

			if($produto['Preco'] == '' || $produto['Preco'] <= 0){
				$errors .= "Preço deve ser um valor numérico maior que zero. \n";
			}
			//if(isset($errors[0])){
			if($errors != ''){
				$errors = "Existem erros no cadastro, gentileza verificar. \n\n".$errors;
				$page = array(
					'ok' => false,
					'error' => $errors,
				);
				echo json_encode($page, JSON_PRETTY_PRINT);
				exit();
			} else{
				try {
					$result = $this->Produto_model->save($produto);
					$page = array(
						'ok' => true,
						'data' => $result,
					);
				} catch (Exception $e) {
					$page = array(
						'ok' => false,
						'error' => 'Ops, aconteceu alguma coisa. Por favor, tente novamente.',
					);
				}
				echo json_encode($page, JSON_PRETTY_PRINT);
				exit();

			}
		}
	}

	public function GetProdutoByCodigo($codigo)
	{
		if($this->Check_session()){
			try {
				$result = $this->Produto_model->GetByCodigo((int) $codigo);
				if($result != null){
					$page = array(
						'ok' => true,
						'data' => $result,
					);
				} else {
					$page = array(
						'ok' => false,
						'error' => 'Produto não cadastrado.',
					);
				}
			} catch (Exception $e) {
				$page = array(
					'ok' => false,
					'error' => 'Ops, aconteceu alguma coisa. Por favor, tente novamente.',
				);
			}
			echo json_encode($page, JSON_PRETTY_PRINT);
			exit();
		}
	}
}
