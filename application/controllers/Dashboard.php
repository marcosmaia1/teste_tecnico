<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
	 	parent::__construct();
		$this->load->helper('url');
		$this->load->model('Usuario_model');
	}

	public function Check_session(){
		$session = $this->session->all_userdata();
		if(!isset($session['usuario'])){
			if($this->input->is_ajax_request()){
				header('Locaction: '.base_url());
			} else {
				$this->load->view('login/login_index');
			}
			return false;
		}
		return true;
	}

	public function index()
	{
		if($this->Check_session()){
	        $dados = array(
	            'result' => $this->session->all_userdata()
	        );
	        $this->load->view('header');
	        $this->load->view('dashboard/dashboard_index', $dados);
	        $this->load->view('footer');
		}
	}
}
