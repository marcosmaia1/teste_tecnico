<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venda extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
	 	parent::__construct();
		$this->load->helper('url');
		$this->load->model('Documento_model');
		$this->load->model('Produto_model');
		$this->load->model('Item_model');
	}

	public function Check_session(){
		$session = $this->session->all_userdata();
		if(!isset($session['usuario'])){
			if($this->input->is_ajax_request()){
				header('Locaction: '.base_url());
			} else {
				$this->load->view('login/login_index');
			}
			return false;
		}
		return true;
	}

	public function index($novo = 0)
	{
		if($this->Check_session()){
			$dados = array(
				'session_data' => $this->session->all_userdata()
			);
			$session = $this->session->all_userdata();
			$usuario_id = $session['usuario']->Tipo == 2 ? $session['usuario']->id : null;
			$totalVendas = $this->Documento_model->GetTotalVendido($usuario_id)->Total;
			$page = array(
				'open_novo_modal' => $novo == 1 ? true : false,
				'lista' => $this->Documento_model->ListAllUser($usuario_id),
				'total' => $totalVendas == null ? 0.0 : $totalVendas,
				//'total' => $this->Documento_model->GetTotalVendido()[0]->Total
			);
			$this->load->view('header', $novo);
			$this->load->view('venda/venda_index', $page);
			$this->load->view('footer');
		}
	}

	public function AdicionaProduto($codigo, $qtd,$documento_id=null)
	{
		if($this->Check_session()){
			try {
				$result = $this->Produto_model->GetByCodigo((int) $codigo);
				if($result != null){
					if($documento_id == null){
						$documento = array(
							'Numero' => 0,
							'Total' => $result->Preco,
							'Confirmado' => false,
							'Usuario_id' => $this->session->all_userdata()['usuario']->id
						);
						$documento = $this->Documento_model->save($documento, true);
						$documento->Numero = $documento->id;
						$this->Documento_model->update(array('id' => $documento->id,'Numero' => $documento->id));
						$item = array(
							'produto_id' => $result->id,
							'documento_id' => $documento->id,
							'Quantidade' => $qtd,
							'Preco' => (float) $result->Preco,
							'Usuario_id' => $this->session->all_userdata()['usuario']->id
						);
						$item = $this->Item_model->save($item, true);
					} else {
						$documento = $this->Documento_model->getById($documento_id);
						if($documento == null){
							$page = array(
								'ok' => false,
								'error' => 'Ops, venda não encontrada. Por favor, tente novamente.',
							);
							echo json_encode($page, JSON_PRETTY_PRINT);
							exit();
						}
						$item = $this->Item_model->ItemExisteNoDocumento($result->id, $documento_id);

						if($item != null){
							$update = array(
								'id' => $item->id,
								'Quantidade' => $qtd
							);
							$this->Item_model->update($update);
							$item->Quantidade = $qtd;
						} else {
							$item = array(
								'produto_id' => $result->id,
								'documento_id' => $documento->id,
								'Quantidade' => $qtd,
								'Preco' => $result->Preco,
								'Usuario_id' => $this->session->all_userdata()['usuario']->id
							);
							$item = $this->Item_model->save($item, true);
						}
					}
					$documento->Total = $this->ReprocessaTotalDocumento($documento->id)->Total;
					$item->Descricao = $result->Descricao;
					$item->Codigo = $result->Codigo;
					$response = array(
						'venda' => $documento,
						'produto' => $item,
					);

					$page = array(
						'ok' => true,
						'data' => $response,
					);
				} else {
					$page = array(
						'ok' => false,
						'error' => 'Produto não cadastrado.',
					);
				}
			} catch (Exception $e) {
				$page = array(
					'ok' => false,
					'error' => 'Ops, aconteceu alguma coisa. Por favor, tente novamente.',
				);
			}
			echo json_encode($page, JSON_PRETTY_PRINT);
			exit();
		}
	}

	public function RemoveProduto($produto_id, $documento_id)
	{
		if($this->Check_session()){
			try {
				$produto_id = (int) $produto_id;
				$item = $this->Item_model->ItemExisteNoDocumento($produto_id, $documento_id);
				if($item != null){
					$this->Item_model->Delete($item->id);
					$documento = $this->ReprocessaTotalDocumento($documento_id);
					$response = array(
						'venda' => $documento,
					);

					$page = array(
						'ok' => true,
						'data' => $response,
					);
				}
			} catch (Exception $e) {
				$page = array(
					'ok' => false,
					'error' => 'Ops, aconteceu alguma coisa. Por favor, tente novamente.',
				);
			}
			echo json_encode($page, JSON_PRETTY_PRINT);
			exit();
		}
	}

	function ReprocessaTotalDocumento($documento_id)
	{
		$item_total = $this->Item_model->GetTotalDocumento($documento_id);
		$update = array(
			'id' => $documento_id,
			'Total' => $item_total
		);
		$this->Documento_model->update($update);
		return $this->Documento_model->getById($documento_id);
	}

	public function UpdateStatusVenda($documento_id, $status)
	{
		if($this->Check_session()){
			try {
				$documento = $this->Documento_model->getById($documento_id);
				if($documento == null){
					$page = array(
						'ok' => false,
						'error' => 'Ops, venda não encontrada. Por favor, tente novamente.',
					);
					echo json_encode($page, JSON_PRETTY_PRINT);
					exit();
				} else {
					$this->ReprocessaTotalDocumento($documento->id)->Total;
					$update = array(
						'id' => $documento_id,
						'Confirmado' => $status,
					);
					$this->Documento_model->update($update);

					$page = array(
						'ok' => true,
						'data' => $status == 1 ? 'Venda confirmada' : 'Venda cancelada',
					);
				}
			} catch (Exception $e) {
				$page = array(
					'ok' => false,
					'error' => 'Ops, aconteceu alguma coisa. Por favor, tente novamente.',
				);
			}
			echo json_encode($page, JSON_PRETTY_PRINT);
			exit();
		}
	}
}
