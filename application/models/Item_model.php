<?php
require_once 'Generico_model.php';

class Item_model extends Generico_model {

    public $Numero;
    public $Total;
    public $Confirmado;
    public $usuario_id;

    public function __construct() {
        $this->NomeTabela = 'item';
	}

    public function ItemExisteNoDocumento($produto_id, $documento_id){
        $this->db->from($this->NomeTabela);
        $this->db->where('documento_id', $documento_id);
        $this->db->where('produto_id', $produto_id);
        $query = $this->db->get();
        foreach ($query->result() as $row)
        {
            return $row;
        }
        return false;
    }

    public function GetTotalDocumento($documento_id){
        $this->db->from($this->NomeTabela);
        $this->db->where('documento_id', $documento_id);
        $query = $this->db->get();
        $total = 0;
        foreach ($query->result() as $row)
        {
            $total += $row->Preco * $row->Quantidade;
        }
        return $total;
        //return $this->db->get();
    }
}
?>
