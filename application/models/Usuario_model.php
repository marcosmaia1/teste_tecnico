<?php
require_once 'Generico_model.php';

class Usuario_model extends Generico_model {

    public $Nome;
    public $Login;
    public $Password;
    public $Tipo;

    public function __construct() {
        $this->NomeTabela = 'usuario';
	}

    public function Login($login, $password){
        return $this->db->get_where($this->NomeTabela, array('login' => $login, 'password' => $password))->row();
    }
}
?>
