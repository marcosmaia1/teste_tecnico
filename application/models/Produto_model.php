<?php
require_once 'Generico_model.php';

class Produto_model extends Generico_model {

    public $Codigo;
    public $Descricao;
    public $Preco;
    public $usuario_id;

    public function __construct() {
        $this->NomeTabela = 'produto';
	}

    public function GetByCodigo($codigo){
        return $this->db->get_where($this->NomeTabela, array('Codigo' => $codigo))->row();
    }
}
?>
