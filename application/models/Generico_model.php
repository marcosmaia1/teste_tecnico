<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Generico_model extends CI_Model {

	public $id;
	public $Criacao;
	public $NomeTabela;

	public function __construct() {

	}

	public function listAll($reverse = null) {
		$this->db->select('`'. $this->NomeTabela .'`.*', FALSE);
		$this->db->from($this->NomeTabela);

		if(!empty($reverse) && $reverse == 'r') {
			$this->db->order_by('id', 'desc');
		}

		return $this->db->get()->result();
	}

	public function getById($id = null) {
		if($id != null ) {
			return $this->db->get_where($this->NomeTabela, array('id' => $id))->row();
		} else {
			return false;
		}
	}

	public function save($dados = null, $returnDados = false) {
		if($dados != null ) {
			$this->db->insert('`'. $this->NomeTabela .'`', $dados);
			if($returnDados){
				return $this->db->get_where($this->NomeTabela, array('id' => $this->db->insert_id()))->row();
			} else {
				return ($this->db->affected_rows() != 1) ? 0 : 1;
			}
		}
	}

	public function update($dados = null) {
		$id = $dados['id'];
		$this->db->where('id', $id);
		$this->db->update($this->NomeTabela, $dados);
	}

	public function delete($id = null) {
		$this->db->where('id', $id);
		$this->db->delete('`'. $this->NomeTabela .'`');
	}
}
