<?php
require_once 'Generico_model.php';

class Documento_model extends Generico_model {

    public $Numero;
    public $Total;
    public $Confirmado;
    public $usuario_id;


    public function __construct() {
        $this->NomeTabela = 'documento';
	}

    public function NumeroExiste($codigo){
        return $this->db->get_where($this->NomeTabela, array('Numero' => $codigo))->row();
    }

    public function GetTotalVendido($usuario_id = null){
        $this->db->select_sum('Total');
        $this->db->from($this->NomeTabela);
        $this->db->where('Confirmado', 1);
        if($usuario_id != null){
            $this->db->where('usuario_id', $usuario_id);
        }
        $query = $this->db->get();
        foreach ($query->result() as $row)
        {
            return $row;
        }
    }

    public function ListAllUser($usuario_id = null, $reverse=''){
        $this->db->select('`'. $this->NomeTabela .'`.*', FALSE);
		$this->db->from($this->NomeTabela);
        if($usuario_id != null){
            $this->db->where('usuario_id', $usuario_id);
        }
		if(!empty($reverse) && $reverse == 'r') {
			$this->db->order_by('id', 'desc');
		}

		return $this->db->get()->result();
    }

    public function GetConfirmadoString($confirmado){
        $Confirmado_string = array(
            0 => 'Em aberto',
            1 => 'Confirmado',
            2 => 'Cancelado'
        );
        return $Confirmado_string[$confirmado];
    }
}
?>
